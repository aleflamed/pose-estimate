#!/bin/env python

# coding: utf-8
import numpy as np
import cv2


class CV(object):
    def __init__(self):
        self.win1 = 'win1'
        cv2.namedWindow(self.win1)
        self.win2 = 'win2'
        cv2.namedWindow(self.win2)
        self.caps = {}

    def open(self, filename):
        if filename == None:
            filename = 0
        if filename in self.caps:
            self.cap = self.caps[filename]
        else:
            self.cap = cv2.VideoCapture(filename)
            self.caps[filename] = self.cap
        self.cap.grab()
        return self.cap

    def grab(self, n):
        ret = []
        while n > 0:
            n -= 1
            frame = self.cap.read()[1]
            if frame is None:
                break
            ret.append(frame)
        return ret

    def _show(self, frames, win):
        for f in frames:
            cv2.imshow(win, f)
            cv2.waitKey(1)

    def show(self, frames):
        self._show(frames, win=self.win1)

    def show2(self, frames):
        self._show(frames, win=self.win2)


def diff(frames):
    return [f2 - f1 for f1, f2 in zip(frames[:-1], frames[1:])]


def slow_threshold(im, val):
    ret = im.copy()
    ret[im >= val] = 255
    ret[im < val] = 0
    return ret


def diffthresh(frames, thresh):
    return [cv2.threshold(cv2.absdiff(f2, f1), thresh, 1000, 0)[1]
        for f1, f2 in zip(frames[:-1], frames[1:])]


def slow_diffthresh(frames, thresh):
    return [slow_threshold(cv2.absdiff(f2, f1), thresh)[1]
        for f1, f2 in zip(frames[:-1], frames[1:])]


class BackgroundIdentify(object):
    K = 5 # number of gaussians kept per pixel

    def __init__(self, width, height):
        self.width = width
        self.height = height
        # Gaussian weights
        self.w = np.zeros(width, height, self.K)
        # Gaussian mean
        self.mean = np.zeros(width, height, self.K)
        # Gaussian variance
        self.var = np.zeros(width, height, self.K)

    def update(self, image):
        pass

def example():
    c = CV()
    c.open('2017-04-14-085453.webm')
    frames = c.grab(500)
    cut1 = frames[170:200]*2 + frames[400:500]
    return c, cut1

if __name__ == '__main__':
    c, cut1 = example()
    c.show(cut1)
