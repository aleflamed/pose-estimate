#!/usr/bin/python

import os
import argparse
import math
import subprocess
from datetime import datetime

import numpy as np
import cv2

import meccanoid
from videofile import VideoFile


array = np.array


# OpenCV Video stream is in BGR.
BLUE = (255, 0, 0)
GREEN = (0, 255, 0)
RED = (0, 0, 255)
PURPLE = (255, 0, 255)


hand_pose = (
    HAND_DOWN,
    HAND_45_DOWN,
    HAND_HORZ,
) = range(3)


# Shoulder centric coordinate system:
# mirrored for left, so only fill in the right shoulder
# 0 - straight angle. positive angle below (up to 90), negative above (down to
# -90)
angle_to_hand_pose = [
    # min, max, hand
    (61, 120, HAND_DOWN),
    (30, 60, HAND_45_DOWN),
    (-120, 29, HAND_HORZ),
]

# own coordinate system - 180 - angle for left -> right
hand_to_angle = {
    HAND_DOWN: 90,
    HAND_45_DOWN: 45,
    HAND_HORZ: 0
}

font = cv2.FONT_HERSHEY_SIMPLEX

hands_to_pose = {
    # left, right, pose
    (HAND_DOWN, HAND_DOWN): meccanoid.POSE_NEXT_TO_BODY,
    (HAND_HORZ, HAND_DOWN): meccanoid.POSE_LEFT_HORIZONTAL,
    (HAND_DOWN, HAND_HORZ): meccanoid.POSE_RIGHT_HORIZONTAL,
    (HAND_45_DOWN,   HAND_DOWN):    meccanoid.POSE_LEFT_DIAG_DOWN,
    (HAND_DOWN, HAND_45_DOWN)  :    meccanoid.POSE_RIGHT_DIAG_DOWN,
    (HAND_HORZ, HAND_HORZ):         meccanoid.POSE_BOTH_HORIZONTAL,
    (HAND_45_DOWN,   HAND_45_DOWN): meccanoid.POSE_BOTH_45_DOWN,
    (HAND_45_DOWN, HAND_HORZ):      meccanoid.POSE_LEFT_DIAG_DOWN_RIGHT_HORZ,
    (HAND_HORZ, HAND_45_DOWN):      meccanoid.POSE_RIGHT_DIAG_DOWN_LEFT_HORZ,
}


# look for haar cascade xml file (different locations in fedora and raspbian)
locations = [
    '/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml',
    '/usr/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml'
    ]
for location in locations:
    if os.path.exists(location):
        haar_cascade_face_xml = location
        break
else:
    print("cannot find haar cascade xml file, please update locations array")
    raise SystemExit


def int_tuple(p):
    return (int(p[0]), int(p[1]))


def slider(title, win, min_val, max_val, initial_val):
    val = [initial_val]
    def op(new_val):
        val[0] = new_val
    cv2.createTrackbar(title, win, min_val, max_val, op)
    cv2.setTrackbarPos(title, win, initial_val)
    return val


class SubprocessMeccanoid(object):
    def __init__(self):
        os.system("killall mecca_process.py 2> /dev/null > /dev/null")
        self.p = subprocess.Popen('./mecca_process.py', stdin=subprocess.PIPE)

    def connect(self):
        self.p.stdin.write("connect\n")

    def pose(self, pose):
        self.p.stdin.write("pose {}\n".format(pose))


class Human(object):
    """
    TODO - Document: what Human contains, and what it is used for.
    """

    HEAD_HEIGHT = 24.0 # centimeters ; taken from personal measurements
    SHOULDER_TO_CENTER_DISTANCE = 15.0
    TOTAL_ARM_LENGTH_FROM_SHOULDER = 90.0
    HEAD_BOTTOM_TO_SHOULDER_HEIGHT = 11.0


    def __init__(self, face):
        self.face = face
        face_w = face[2]
        face_h = face[3]
        face_x = face[0] + face_w // 2
        face_y = face[1] + face_h // 2
        self.face_center = face_center = array([face_x, face_y])
        r = self.ratio = face_h / self.HEAD_HEIGHT
        d = r * array(
            [-self.SHOULDER_TO_CENTER_DISTANCE,
             self.HEAD_HEIGHT // 2 + self.HEAD_BOTTOM_TO_SHOULDER_HEIGHT])
        self.left_shoulder = int_tuple(face_center + d)
        self.right_shoulder = int_tuple(face_center + [-d[0], d[1]])
        self.arm_length = int(self.TOTAL_ARM_LENGTH_FROM_SHOULDER * r)


    def shoulders(self):
        return [self.left_shoulder, self.right_shoulder]


class VideoStream(object):

    def __init__(self, filename, on_frame, winname, fps=10):
        self.cap = cv2.VideoCapture(filename)
        self.cap.set(cv2.CAP_PROP_FPS, fps)
        self.on_frame = on_frame
        self.frame_number = 0


    def get_frame(self, pos=None):
        self.frame_number += 1
        frame = self.cap.read()[1]
        return frame


    def release(self):
        self.cap.release()


    def on_key(self, c):
        frame = self.get_frame()
        self.on_frame(self.frame_number, frame)


def make_video_object(filename, on_frame, winname, fps):
    if filename[:5] == '/dev/':
        return VideoStream(filename, on_frame=on_frame, winname=winname, fps=fps)
    # Note - a file has it's own fps, which is ignored here, but should be print
    return VideoFile(filename, on_frame, winname=winname)


def rects_on_image(img, rects, color):
    for x, y, w, h in rects:
        cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)


def center_of_mass(bin):
    mom = cv2.moments(bin, True)
    m00 = mom['m00']
    if abs(m00) < 1e-3:
        return None
    # return x, y
    return mom['m10'] / m00, mom['m01'] / m00


def mask_circle(mat, c, r):
    cx, cy = c
    y, x = np.indices(mat.shape)
    mat[(x - cx)**2 + (y - cy)**2 <= r**2] = 0


def mask_circle_neg(mat, c, r):
    cx, cy = c
    y, x = np.indices(mat.shape)
    mat[(x - cx)**2 + (y - cy)**2 >= r**2] = 0


def hands_via_center_of_mass(binary, human):
    # left
    left_image = binary.copy()
    left_image[:, human.left_shoulder[0]:] = 0
    mask_circle(left_image, human.left_shoulder, human.arm_length * 0.5)
    mask_circle_neg(left_image, human.left_shoulder, human.arm_length * 1.0)
    # right
    right_image = binary.copy()
    right_image[:, 0 : human.right_shoulder[0]] = 0
    mask_circle(right_image, human.right_shoulder, human.arm_length * 0.5)
    mask_circle_neg(right_image, human.right_shoulder, human.arm_length * 1.0)
    return dict(left=dict(max=center_of_mass(left_image)), right=dict(max=center_of_mass(right_image)))


def find_arm_angles(human, binary_src, factor):
    binary = binary_src

    # mask out everthing below the shoulders
    binary[0 : human.left_shoulder[1], :] = 0

    d = hands_via_center_of_mass(binary, human)

    def paint(frame):
        for _p in human.shoulders():
            p = tuple(int(x * factor) for x in _p)
            cv2.circle(frame, p, 5, GREEN)
            cv2.circle(frame, p, int(human.arm_length * factor), GREEN)
        for which, com_color, shoulder in [('left', BLUE, human.left_shoulder), ('right', GREEN, human.right_shoulder)]:
            p = d[which]['max']
            if p is None:
                continue
            com = tuple(int(x * factor) for x in p)
            cv2.circle(frame, com, 20, com_color)
            cv2.line(frame, tuple(int(factor * x) for x in shoulder), com, GREEN)
    return paint, d


def majority(t):
    if all(t) == t[0]:
        return t[0]
    return None


class Controller(object):


    MAX_FACE_MOVE = 5 # pixels from last position, regardless of time difference


    def __init__(self, ticks_per_second, use_meccanoid=False, debug=False):
        self.ticks_per_second = ticks_per_second
        self.debug = debug
        self.face_location = None # last location face was seen
        self.face_time = None # time face was last seen

        self.left_hand_raw = None # last position of left hand
        self.left_time = None # last time left hand seen
        self.left_angle = 90 # assume hand down as initial position
        self.left_last = None

        self.right_hand_raw = None # last position of right hand
        self.right_time = None # last time right hand seen
        self.right_angle = 90 # assume hand down as initial position
        self.right_last = None

        self.t = 0
        self.frame_number = 0
        self.binary = None

        if debug:
            cv2.namedWindow("out", cv2.WINDOW_AUTOSIZE)
            cv2.moveWindow("out", 0, 650)

        # image processing variables
        self.ones_3_kernel = np.ones((5, 5))
        default_erosion = 1
        if self.debug:
            self.erosion = slider("Erosions", "out", 0, 3, default_erosion)
        else:
            self.erosion = [default_erosion]

        self.learning_rate_max = 0.01
        self.learning_rate_options = 10
        default_learning_rate = 0
        if self.debug:
            self.learning_rate = slider('learning rate', 'out', 0, self.learning_rate_options, default_learning_rate)
        else:
            self.learning_rate = [default_learning_rate]

        self.face_cascade = cv2.CascadeClassifier(haar_cascade_face_xml)

        self.bg = cv2.createBackgroundSubtractorMOG2(history=4000)

        self.use_meccanoid = use_meccanoid
        if self.use_meccanoid:
            self.me = SubprocessMeccanoid()
            self.me.connect()

        self._last_pose = None

        self.num_poses = 5
        self.recorded_left = [HAND_DOWN] * self.num_poses
        self.recorded_right = [HAND_DOWN] * self.num_poses


    def debug_print(self, s):
        print("{}: {}".format(self.frame_number, s))


    def selected_left(self):
        return first_not_none(majority(self.recorded_left), self.left_last)


    def selected_right(self):
        return first_not_none(majority(self.recorded_right), self.right_last)


    def selected_pose(self):
        # comment out for mirror
        #return get_pose_from_hands(self.selected_right(), self.selected_left())
        return get_pose_from_hands(self.selected_left(), self.selected_right())


    def record_pose(self, left, right):
        if left is not None:
            self.recorded_left = self.recorded_left[1:] + [left]
        if right is not None:
            self.recorded_right = self.recorded_right[1:] + [right]
        self.debug_print("{!r} {!r}".format(self.recorded_left, self.recorded_right))
        self.set_pose(self.selected_pose())


    def set_pose(self, pose):
        if self._last_pose == pose or pose is None:
            return
        self._last_pose = pose
        self.debug_print("                                  setting {}".format(meccanoid.pose_dict[pose]))
        if self.use_meccanoid:
            self.me.pose(pose)


    def update_face_location(self, human):
        if self.face_location is None:
            self.face_location = human.face_center
            return False
        dist = np.linalg.norm(human.face_center - self.face_location)
        self.face_location = human.face_center
        self.face_time = self.t
        if dist > self.MAX_FACE_MOVE:
            print("face moved too much ({} > {}), ignoring".format(dist, self.MAX_FACE_MOVE))
            return False
        return True


    def process(self, frame_number, frame):
        start = datetime.now()
        self.process_inner(frame_number, frame)
        dt = (datetime.now() - start).total_seconds()
        self.debug_print("time {}".format(dt))
        if self.debug:
            cv2.imshow("out", self.binary)


    def process_inner(self, frame_number, frame):
        """
        on every frame at time t:

        1. look for a face - if none found, or more than one is found, ignore frame

        2. let FACE be the found face

        3. if dist(FACE[t].position, FACE[last_t].position) > MAX_FACE_MOVE:
            update position, ignore frame

        4. if no change detected (stationary for 1 frame), and move detected in previous frame:
           give command to robot based on last recorded positions of left and right arm (can be a nope - if the same position is given)

        5. if movement is detected, record newly found locations of left and right (might only be movement of one)

        A few measures for reducing CPU requirements: (10 FPS at the time of writing on the pi)
         - look for image in top half, middle third (1/6 of the pixels)
         - face detection is done in full resolution, but background detection
           is done on a quarter of it (and so is the rest of the logic)
           (1/4 of the pixels)
        """
        self.frame_number = frame_number
        self.factor = factor = 2
        self.t += 1
        k = self.ones_3_kernel

        # step 1: convert to grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # step 2: down scale by a factor of 2 on each axis
        if factor == 2:
            gray22 = cv2.pyrDown(gray)
        else:
            gray22 = gray

        # step 3: background detection, erosion and thresholding
        lr = self.learning_rate_max * 0.1 ** self.learning_rate[0]
        out = self.bg.apply(gray22, None, lr) # middle - mask
        out = cv2.erode(out, k, iterations=self.erosion[0])
        something, binary = cv2.threshold(out, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        self.binary = binary

        # step 4: face detection
        faces = self.face_detect(gray)
        rects_on_image(frame, faces, GREEN)
        if len(faces) != 1:
            if len(faces) == 0:
                print("no face - ignoring")
            else:
                print("too many faces - only 1 supported")
            return binary

        # step 5: ignore frame if faced moved too much
        face = tuple(x/2 for x in faces[0])
        self.human = human = Human(face)

        if not self.update_face_location(human):
            return binary

        # step 6: look for arms and return found positions
        paint, d = find_arm_angles(human, binary, factor=factor)
        paint(frame)

        # step 7: update hands locations and angles
        for side, updater in [
            ('left', self.update_left),
            ('right', self.update_right)]:
            updater(d[side].get('max', None))

        # step 8: find hand poses from angles
        left_hand = find_hand_pose(self.left_angle)
        right_hand = find_hand_pose(self.right_angle)

        # debugging visual aids
        cv2.putText(frame, "L {}".format(self.left_angle), (10, 400), font, 1, RED)
        cv2.putText(frame, "R {}".format(self.right_angle), (10, 440), font, 1, RED)
        cv2.putText(frame, "{!r} | {}".format(self.recorded_left, self.left_last), (10, 40), font, 1, PURPLE)
        cv2.putText(frame, "{!r} | {}".format(self.recorded_right ,self.right_last), (10, 80), font, 1, PURPLE)
        self.draw_pose(frame)

        # step 9: move robot to found location
        self.record_pose(left_hand, right_hand)

        return binary


    def draw_pose(self, frame):
        if self.left_angle is None and self.right_angle is None:
            return
        human = self.human
        factor = self.factor
        l = human.arm_length
        for color, width, left, right in [
            (RED, 1, self.recorded_left[-1], self.recorded_right[-1]),
            (PURPLE, 3, self.selected_left(), self.selected_right())]:
            for p, hand, converter in [(human.left_shoulder, left, lambda x: 180 - x), (human.right_shoulder, right, lambda x: x)]:
                if hand is None:
                    continue
                a = converter(hand_to_angle[hand])
                a = a * math.pi / 180.0
                target = int_tuple([factor * (p[0] + l * math.cos(a)),
                                    factor * (p[1] + l * math.sin(a))])
                source = tuple(x * factor for x in p)
                cv2.line(frame, source, target, color, width)


    def face_detect(self, gray):
        if self.t % 3 == 0 or not hasattr(self, 'last_faces'):
            height, width = gray.shape
            left, right, bottom, top = width // 3, width * 2 // 3, 0, height // 2
            cropped = gray[bottom : top, left : right]
            cropped_faces = self.face_cascade.detectMultiScale(cropped, 1.3, 5)
            faces = [(x + left, y + bottom, w, h) for (x, y, w, h) in cropped_faces]
            self.last_faces = faces
        else:
            faces = self.last_faces
        return faces


    def update_left(self, hand):
        self.left_angle_raw = hand
        if hand is None:
            self.left_angle = hand
            return
        self.left_time = self.t
        self.left_angle = constrain(-90, 90, angle_from_points(x_mirrored(self.human.left_shoulder), x_mirrored(hand), lambda s: self.debug_print('L' + s)))
        self.left_last = find_hand_pose(self.left_angle)


    def update_right(self, hand):
        self.right_angle_raw = hand
        if hand is None:
            self.right_angle = None
            return # what to do?
        self.right_time = self.t
        self.right_angle = constrain(-90, 90, angle_from_points(self.human.right_shoulder, hand, lambda s: self.debug_print('R ' + s)))
        self.right_last = find_hand_pose(self.right_angle)


def first_not_none(a, b):
    return b if a is None else a


def constrain(minimal, maximal, v):
    return min(maximal, max(minimal, v))


def x_mirrored((x, y)):
    return (-x, y)


def angle_from_points(p1, p2, debug=None):
    """ returns angle in radians """
    dx, dy = [a - b for a, b in zip(p2, p1)]
    ang = math.atan2(dy, dx) / math.pi * 180
    if debug:
        debug("{!r}->{!r}, {}".format(p1, p2, ang))
    return ang


def find_hand_pose(angle):
    if angle is None:
        return None
    for minimum, maximum, pose in angle_to_hand_pose:
        if minimum <= angle and angle <= maximum:
            return pose
    return None


def get_pose_from_hands(left, right):
    return hands_to_pose.get((right, left), None)

# ################################################################################

def main(filename, move, debug, fps):
    cv2.namedWindow("video", cv2.WINDOW_AUTOSIZE)
    cv2.moveWindow("video", 0, 0)

    def to_key(val):
        if val == -1:
            return None
        return chr(val)

    controller = Controller(ticks_per_second=fps, use_meccanoid=move, debug=debug)

    def on_frame(frame_number, frame):
        controller.process(frame_number, frame)
        cv2.imshow("video", frame)

    video = make_video_object(filename, on_frame, fps=fps, winname='video')

    while True:
        c = to_key(cv2.waitKey(1))
        video.on_key(c)
        if c == 'q':
            print("quitting")
            break
    video.release()
    cv2.destroyAllWindows()


def args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename', default='/dev/video0')
    parser.add_argument('--move', default=False, action='store_true')
    parser.add_argument('--debug', default=False, action='store_true')
    parser.add_argument('--fps', type=int, default=10)
    return parser.parse_args()


if __name__ == '__main__':
    # I have seen gatttool taking all cpu, so kill them early
    os.system("killall gattool 2> /dev/null > /dev/null")
    a = args()
    main(filename=a.filename, move=a.move, debug=a.debug, fps=a.fps)
