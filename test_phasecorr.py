import numpy as np

import cv2

import videofile

def phasecorrelate(frames):
    gray_float = [np.float32(cv2.cvtColor(frames[i, :, :, :], cv2.COLOR_BGR2GRAY)) for i in range(frames.shape[0])]
    return [cv2.phaseCorrelate(gray_float[i], gray_float[i + 1]) for i in range(len(gray_float) - 1)]


def gray_to_shift(frame):
    return np.float32(frame)
    # try gaussian - but reduces correlation significantly
    #cv2.GaussianBlur(first_gray_f32, (1, 1), 0)

def frame_to_gray(f):
    return cv2.pyrDown(cv2.cvtColor(f, cv2.COLOR_BGR2GRAY))


def main():
    cv2.namedWindow('source')
    cv2.namedWindow('target')
    cv2.moveWindow('source', 0, 0)
    cv2.moveWindow('target', 640, 0)
    v = videofile.VideoFile('with_self_shake.avi')
    frame_iter = v.as_iter()
    for i, f in enumerate(frame_iter):
        if i % 30 == 0:
            ref = frame_iter.next()
            ref_gray_f32 = gray_to_shift(frame_to_gray(ref))
        gray = frame_to_gray(f)
        gray_f32 = gray_to_shift(gray)
        shift, power = cv2.phaseCorrelate(ref_gray_f32, gray_f32)
        if power < 0.2:
            print("{} skipping".format(i))
        M = np.float32([[1, 0, -shift[0]], [0, 1, -shift[1]]])
        rows, cols = gray.shape
        gray_moved = cv2.warpAffine(gray, M, (cols, rows))
        cv2.imshow("source", gray)
        cv2.imshow("target", gray_moved)
        c = cv2.waitKey(10)
        if c in [27, ord('q')]:
            return


if __name__ == '__main__':
    main()
