import meccanoid



def run_through_chest_light_combinations(mec):
    for i in range(16):
        for j in range(4):
            mec.chest_light(j, i & (1 << j))


def main():
    m = meccanoid.Meccanoid()

    # Put Meccanoid into blue tooth mode - it will stop moving on it's own and set arms to sides
    m.connect()


if __name__ == '__main__':
    main()
