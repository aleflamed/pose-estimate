#!/usr/bin/python

import sys

import numpy as np
import cv2


def main(filename):
    cap = cv2.VideoCapture(filename)

    print("press q to quit (ctrl-c also works)")
    while(cap.isOpened()):
        ret, frame = cap.read()

        if not ret:
            break

        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv[-1])
