import time

from pygatt.exceptions import NotConnectedError

import pygatt

# ----------------------------------------------------------------------

# The servo IDs, some are unused as yet
UNKNOWN0_SERVO       = 0
RIGHT_ELBOW_SERVO    = 1
RIGHT_SHOULDER_SERVO = 2
LEFT_SHOULDER_SERVO  = 3
LEFT_ELBOW_SERVO     = 4
UNKNOWN5_SERVO       = 5
UNKNOWN6_SERVO       = 6
UNKNOWN7_SERVO       = 7

# The handle to use when sending commands
_HANDLE = 0x001f


# POSE constants
POSES = (
POSE_NEXT_TO_BODY,
POSE_LEFT_HORIZONTAL,
POSE_RIGHT_HORIZONTAL,
POSE_BOTH_HORIZONTAL,
POSE_LEFT_DIAG_DOWN,
POSE_RIGHT_DIAG_DOWN,
POSE_BOTH_45_DOWN,
POSE_LEFT_DIAG_DOWN_RIGHT_HORZ,
POSE_RIGHT_DIAG_DOWN_LEFT_HORZ
) = range(9)

pose_dict = {globals()[x]: x for x in globals().keys() if x[:5] == 'POSE_'}

# ----------------------------------------------------------------------

class Meccanoid(object):
    """
    Simple class for controlling the Meccanoid
    """
    def __init__(self):
        """
        Constructor.
        """
        super(Meccanoid, self).__init__()

        self._gatt = pygatt.GATTToolBackend()
        self._gatt.start()
        self._device = None

        # These are stateful so remember them and mutate accordingly
        self._servos = \
            [0x08,
             0x7f, 0x80, 0x00, 0xff, 0x80, 0x7f, 0x7f, 0x7f,
             0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01]


    def connect(self, address='C4:BE:84:3B:9D:90'):
        """
        Connect to the Meccanoid at the given Bluetooth address.

        """

        if self._device is None:
            # Connect to it
            self._device = self._gatt.connect(address)

            # Send a wheel move of zero to make the meccanoid see us
            self.move(0, 0)

            # Put arms into their start state
            self.pose(POSE_NEXT_TO_BODY)


    def disconnect(self):
        """
        Disconnect from the Meccanoid.
        """
        if self._device is not None:
            self._device.disconnect()
            self._device = None


    _poses = {
            POSE_NEXT_TO_BODY: [
                 (RIGHT_ELBOW_SERVO, 35),
                 (RIGHT_SHOULDER_SERVO,0 ),
                 (LEFT_ELBOW_SERVO, 0),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_LEFT_HORIZONTAL: [
                 (RIGHT_ELBOW_SERVO, 35),
                 (RIGHT_SHOULDER_SERVO,0 ),
                 (LEFT_ELBOW_SERVO, 175),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_RIGHT_HORIZONTAL: [
                 (RIGHT_ELBOW_SERVO, 190),
                 (RIGHT_SHOULDER_SERVO,0 ),
                 (LEFT_ELBOW_SERVO, 0),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_BOTH_HORIZONTAL: [
                 (RIGHT_ELBOW_SERVO, 190),
                 (RIGHT_SHOULDER_SERVO,0 ),
                 (LEFT_ELBOW_SERVO, 175),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_LEFT_DIAG_DOWN: [
                 (RIGHT_ELBOW_SERVO, 35),
                 (RIGHT_SHOULDER_SERVO,0),
                 (LEFT_ELBOW_SERVO, 80),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_RIGHT_DIAG_DOWN: [
                 (RIGHT_ELBOW_SERVO, 100),
                 (RIGHT_SHOULDER_SERVO,0),
                 (LEFT_ELBOW_SERVO, 0),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_BOTH_45_DOWN: [
                 (RIGHT_ELBOW_SERVO, 100),
                 (RIGHT_SHOULDER_SERVO,0),
                 (LEFT_ELBOW_SERVO, 80),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_LEFT_DIAG_DOWN_RIGHT_HORZ: [
                 (RIGHT_ELBOW_SERVO, 190),
                 (RIGHT_SHOULDER_SERVO,0),
                 (LEFT_ELBOW_SERVO, 80),
                 (LEFT_SHOULDER_SERVO,0)
                ],
            POSE_RIGHT_DIAG_DOWN_LEFT_HORZ: [
                 (RIGHT_ELBOW_SERVO, 100),
                 (RIGHT_SHOULDER_SERVO,0),
                 (LEFT_ELBOW_SERVO, 175),
                 (LEFT_SHOULDER_SERVO,0)
                ],
        }

    def pose(self, p):
        self.servo(self._poses[p])
        self._send(self._servos)


    def servo(self, servo, value=None):
        """
        Set a servo value.

        The servo numbers are constants in this module.
        The value is between 0x00 and 0xff.
        """
        if value is None:
            poses = servo
        else:
            poses = [(servo, value)]

        for servo, value in poses:
            self._set_servo(servo, value)


    def _set_servo(self, servo, value):

        servo = int(servo)

        if 0 <= servo and servo <= 7:
            # Cap to a byte
            value = self._cap(int(value))

            # These guys are reversed, handle that for the user
            if (servo == LEFT_SHOULDER_SERVO or
                servo == RIGHT_ELBOW_SERVO) and value != 0x80:
                value = 0xff - value

            # Set the values
            self._servos[servo + 1] = value

        else:
            raise ValueError("Bad servo index: %s" % servo)


    def move(self, right_speed=0x00, left_speed=0x00):
        """
        Move the wheels.
        """

        # By default do nothing
        right_dir = 0x00
        left_dir  = 0x00

        # Make your move
        if right_speed > 0:
            right_dir   = 0x01
            right_speed = self._cap(right_speed)
        else:
            right_dir   = 0x02
            right_speed = self._cap(-right_speed)

        if left_speed > 0:
            left_dir   = 0x01
            left_speed = self._cap(left_speed)
        else:
            left_dir   = 0x02
            left_speed = self._cap(-left_speed)

        # Send the command
        self._send((0x0d, left_dir, right_dir, int(left_speed), int(right_speed),
                    0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00))


    def _send(self, values):
        """
        Send a command to the unit.
        """

        if self._device is None:
            raise NotConnectedError()

        try:
            checksum = 0
            for v in values:
                checksum += v

            payload = tuple(values) + ((checksum >> 8) & 0xff, checksum & 0xff)

            self._device.char_write_handle(_HANDLE, payload)

        except NotConnectedError:
            self._device = None
            raise


    def _cap(self, value):
        """
        Cap between 0x00 and 0xff.
        """
        return max(0x00, min(0xff, value))


def demo_poses():
    m = Meccanoid()
    m.connect()
    time.sleep(2)
    def set_pose(pose):
        print("setting {}".format(pose_dict[pose]))
        m.pose(pose)
        #time.sleep(2)
    for pose in POSES * 100 + [POSE_NEXT_TO_BODY]:
        set_pose(pose)
    m.disconnect()


if __name__ == '__main__':
    demo_poses()
