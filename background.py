#!/usr/bin/python

import sys

import numpy as np
import cv2


def main(filename):
    cap = cv2.VideoCapture(filename)

    cv2.namedWindow('frame')
    #cv2.namedWindow('third')

    #mog2 = cv2.createBackgroundSubtractorMOG2()
    hog=cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    print("press q to quit (ctrl-c also works)")
    while(cap.isOpened()):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        if not ret:
            break

        (rects, weights) = hog.detectMultiScale(frame)
        for (x, y, w, h) in rects:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.imshow('frame', frame)

        #res = mog2.apply(gray)
        #cv2.imshow('third', res)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv[-1])
