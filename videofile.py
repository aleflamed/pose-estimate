import os
import sys
from contextlib import contextmanager
from datetime import datetime

import cv2
import numpy as np


def make_circle(height, width, cx, cy, r, v):
    ret = np.zeros((height, width))
    x, y = np.indices(ret.shape)
    ret[(x - cx)**2 + (y - cy)**2 <= r**2] = v
    return ret


def fake():
    width = 640
    height = 480
    frames = 100
    ret = np.zeros((frames, height, width, 3), dtype=np.uint8)
    circle = make_circle(height, width, height // 2, width // 2, height // 4, 255)
    for i in range(50, 100):
        ret[i, :, :, 0] = circle
        ret[i, :, :, 1] = circle
        ret[i, :, :, 2] = circle
    return ret



def npcached(f):
    def wrapper(*args, **kw):
        if 'filename' not in kw:
            filename = args[0]
        basename = os.path.basename(filename)
        cache_filename = 'cache.{}.npy'.format(basename)
        if os.path.exists(cache_filename):
            with timed("loading {} ({} bytes)".format(cache_filename, os.stat(cache_filename).st_size)):
                return np.load(cache_filename)
        ret = f(*args, **kw)
        assert isinstance(ret, np.ndarray)
        with timed("saving {} ({} elements)".format(cache_filename, ret.size)):
            np.save(cache_filename, ret)
        return ret
    return wrapper


@contextmanager
def timed(label):
    start = datetime.now()
    print("starting {}".format(label))
    yield
    print("total: {}".format(datetime.now() - start))


@npcached
def readall(filename):
    with timed("reading {}".format(filename)):
        cap = cv2.VideoCapture(filename)
        frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        cap.grab()
        ret = np.zeros((frames, height, width, 3), dtype=np.uint8)
        for i in range(frames):
            sys.stdout.write("{}/{}\r".format(i + 1, frames))
            sys.stdout.flush()
            has_next, frame = cap.read()
            if has_next:
                ret[i, :, :] = frame
        return ret


def read_video(filename):
    if filename == 'fake':
        video = fake()
    else:
        video = readall(filename)
    return video


class VideoFile(object):
    def __init__(self, filename, on_frame=None, winname=None):
        self.winname = winname
        self.video = read_video(filename)
        self.frames = len(self.video)
        self.pos = 0
        self.run = True
        self.step_key = 's'
        self.prev_key = 'p'
        self.keymap = dict(
            r=self.on_toggle_run,
            e=self.on_restart,
            h=self.on_help
        )
        self.keymap[self.step_key] = self.on_step
        self.keymap[self.prev_key] = self.on_step
        self.on_frame = on_frame
        self.create_slider()

    def release(self):
        pass

    def get_num_frames(self):
        return self.frames

    def as_iter(self):
        i = 0
        while i < self.frames:
            yield self.video[i, :, :, :]
            i += 1

    def get_frame(self, pos=None):
        if pos is None:
            pos = self.pos
        return self.video[pos].copy()

    def create_slider(self):
        if self.winname is not None:
            cv2.createTrackbar("Pos", self.winname, 0, self.frames - 1, self.on_trackbar_change)

    def update_and_set_trackbar(self, pos):
        if self.winname is not None:
            cv2.setTrackbarPos("Pos", self.winname, pos)
        else:
            self.on_trackbar_change(pos)

    def on_trackbar_change(self, pos):
        self.pos = pos

    def _advance(self, dpos=1):
        self.update_and_set_trackbar(max(0, min(self.pos + dpos, self.frames - 1)))
        if self.on_frame:
            self.on_frame(self.pos, self.get_frame())

    def on_key(self, c):
        if c in self.keymap:
            self.keymap[c]()
        if (c is None and self.run) or c == self.step_key:
            self._advance()
        if c == self.prev_key:
            self._advance(-1)

    def on_help(self):
        print("\n".join('{}: {}'.format(k, v.__doc__) for k, v in self.keymap.items()))

    def on_toggle_run(self):
        self.run = not self.run

    def on_step(self):
        self.run = False

    def on_restart(self):
        self.pos = 0
