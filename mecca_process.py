#!/usr/bin/python

import meccanoid


def main():
    me = meccanoid.Meccanoid()
    while True:
        try:
            line = raw_input()
        except EOFError:
            break
        args = line.split()
        command = args[0]
        try:
            params = map(int, args[1:])
        except:
            continue
        if not hasattr(me, command):
            continue
        getattr(me, command)(*params)
    me.disconnect()


if __name__ == '__main__':
    main()
